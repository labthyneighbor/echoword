
helm upgrade cert-manager   --namespace cert-manager   --version v0.13.0   jetstack/cert-manager
helm upgrade echo echochart
helm upgrade nginx stable/nginx-ingress
helm install mongo stable/mongodb -f values/mongodb.yml
# ^^ to use mongodb, assert values.yaml mongodb: enabled:

curl https://kubersoldat.mooo.com/?input=hey